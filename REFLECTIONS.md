### Oppgave 6 c)
Vi noter ned ressurssbruk som virker suspekt.
* Hver gang man gjør en GET request for å få siden blir load på CPU-en generelt høy.
* Systemet er veldig utsatt for DOS angrep der resursbruken på CPU-en går kjapt opp til 100%.
* Når vi lager en ny bruker tar det et stort hopp 
* POST request skjer det ingen ting unormalt
* Unaturlig høy minnebruk. 2.5 GB for en webserver er gangske mye. Her bør man se på hva som er normalt og sammenligne det med en annen lignende applikasjon.

