document.getElementById("newUsername").value = sessionStorage.getItem("username");
document.getElementById("cancelButton").addEventListener("click", function(){
    window.location.href = "../app.html";
});

/**
 * Makes HTTP PUT request to server for updating username and password
 */
function editUser (event) {
    event.preventDefault();
    let newInformation = {
        "userId": sessionStorage.getItem("userId"),
        "username": document.getElementById('newUsername').value,
        "password": document.getElementById('newPassword').value
    };

    fetch('../api/user/'+ sessionStorage.getItem("userId"), {
        method: "PUT",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(newInformation)
    })
        .then(response => response.json())
        .then(response => {
            if (response === true) {
                alert("Bruker oppdatert");
                loadApplication(newInformation)
            } else {
                alert("Brukernavn eksisterer fra før, vennligst skriv inn et nytt brukernavn");
            }
        })
        .catch(error => console.error(error));
}


/**
 * Adds user information to sessionStorage and redirects to app.html
 * @param user contains user information as a JSON object
 */
 function loadApplication(user){
    sessionStorage.setItem("userId", user.userId);
    sessionStorage.setItem("username", user.username);
    window.location.href = "../app.html"

}