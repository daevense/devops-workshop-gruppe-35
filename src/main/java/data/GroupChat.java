package data;

import java.util.ArrayList;

/**
 * Class for the GroupChat object as saved in database
 */
public class GroupChat {
    private int groupChatId;
    private String groupChatName;
    private ArrayList<Message> messageList;
    private ArrayList<User> userList;

    public GroupChat() {
        this.groupChatId = 0;
        this.groupChatName = "";
        this.messageList = new ArrayList<Message>();
        this.userList = new ArrayList<User>();
    }


    public GroupChat(int groupChatId, String groupChatName) {
        this.groupChatId = groupChatId;
        this.groupChatName = groupChatName;
        this.messageList = new ArrayList<>();
        this.userList = new ArrayList<>();
    }

    public int getGroupChatId() {
        return groupChatId;
    }

    public String getGroupChatName() {
        return groupChatName;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public void setGroupChatId(int groupChatId) {
        this.groupChatId = groupChatId;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }

    public void setGroupChatName(String groupChatName) {
        this.groupChatName = groupChatName;
    }

    public void setMessageList(ArrayList<Message> messageList) {
        this.messageList = messageList;
    }
}


