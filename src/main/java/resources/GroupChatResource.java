package resources;

import data.GroupChat;
import dao.GroupChatDAO;
import data.Message;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        GroupChat chat = groupChatDAO.getGroupChat(groupChatId);
        chat.setMessageList(groupChatDAO.getGroupChatMessages(groupChatId));
        chat.setUserList(groupChatDAO.getGroupChatUsers(groupChatId));
        return chat;
        /*
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        groupChatDAO.getGroupChat(groupChatId);
        groupChatDAO.getGroupChatMessages(groupChatId);
        groupChatDAO.getGroupChatUsers(groupChatId);
        return ;*/
    }

    @GET
    @Path ("user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatByUserId(@PathParam("userId") int userId) {
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        ArrayList<GroupChat> chat = groupChatDAO.getGroupChatByUserId(userId);
        return chat;
    }

    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat) {
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.addGroupChat(groupChat);
    }

    @GET
    @Path ("{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> getGroupChatMessages(@PathParam("groupChatId") int groupChatId) {
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path ("{groupChatId}/message")
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message) {
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.addMessage(groupChatId, message);
    }

}
