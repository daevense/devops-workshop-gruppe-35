package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");

        String result = "OBS! Det har skjedd en feil :(";

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */
         
        if(expressionTrimmed.matches("^\\d+(?:\\s*[+\\-*/^]\\s*\\d+)*$")) {
            int answer = sum(expressionTrimmed);
            if (answer == 69) return "nice";
            return Integer.toString(answer);
        }
        return result;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sum(String expression){
        String[] split = expression.split("[+]");
        int result = 0;
        for (String string : split) result += subtraction(string);

        return result;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int subtraction(String expression){
        String[] split = expression.split("[-]");
        int result = multiplication(split[0]);
        for (int i = 1; i < split.length; i++) {
            result -= multiplication(split[i]);
        }

        return result;
    }

    /**
     * Method used to calculate a product expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int multiplication(String expression){
        String[] split = expression.split("[*]");

        int result = 1;
        for (String string : split) result *= division(string);

        return result;
    }

    /**
     * Method used to calculate a division expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as a double
     */
    public double division(String expression){
        String[] split = expression.split("[/]");

        double result = power(split[0]);
        for (int i = 1; i < split.length; i++) {
            result /= power(split[i]);
        }

        return result;
    }

    /**
     * Method used to calculate a power expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int power(String expression){
        String[] split = expression.split("[\\^]");

        int base = Integer.parseInt(split[0]);
        int exponent = 1;
        for (int i = 1; i < split.length; i++) {
            exponent *= Integer.parseInt(split[i]);
        }

        return pow(base, exponent);
    }

    /**
     * Calculates an integer power.
     * @param b the base of the power
     * @param p the exponent of the power
     * @return an integer power
     */
    public static int pow(int b, int p) {
        int result = 1;
        while (p != 0) {
            if ((p & 1) == 1) result *= b;
            b *= b;
            p >>>= 1;
        }
        return result;
    }
}
