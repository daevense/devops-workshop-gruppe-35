import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        String equation = " 10 * 6 ";
        assertEquals("60", calculatorResource.calculate(equation));

        equation = "49/7";
        assertEquals("7", calculatorResource.calculate(equation));

        equation = "2+5*4-6^2*5/3";
        assertEquals("-38", calculatorResource.calculate(equation));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "5+9+1";
        assertEquals(15, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));

        expression = "20-2-9";
        assertEquals(9, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "10*10";
        assertEquals(100, calculatorResource.multiplication(equation));

        equation = "6*6";
        assertEquals(36, calculatorResource.multiplication(equation));

        equation = "6*6*5";
        assertEquals(180, calculatorResource.multiplication(equation));
    }

    @Test
    public void testDivision() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "10/10";
        assertEquals(1, (int) calculatorResource.division(equation));

        equation = "70/5";
        assertEquals(14, (int) calculatorResource.division(equation));

        equation = "70/5/2";
        assertEquals(7, (int) calculatorResource.division(equation));
    }

    @Test
    public void testPower() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "2^1";
        assertEquals(2, calculatorResource.power(equation));

        equation = "6^7";
        assertEquals(279936, calculatorResource.power(equation));

        equation = "1^9";
        assertEquals(1, calculatorResource.power(equation));

        equation = "2^3^4";
        assertEquals(4096, calculatorResource.power(equation));
    }

    @Test
    public void testPow() {
        assertEquals(4, CalculatorResource.pow(2, 2));

        assertEquals(1953125, CalculatorResource.pow(5, 9));

        assertEquals(81, CalculatorResource.pow(3, 4));
    }
}
